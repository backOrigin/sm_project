package com.smproject.enums.checks;

/**
 * @author hu.liang
 * @Date 2019/6/29 0029
 */
public enum PhoneRegex  {

    /**
     * 手机号码的正则表达式
     */
    PHONE_REGEX_TELEPHONE("^[1](([3|5|8][\\d])|([4][4,5,6,7,8,9])|([6][2,5,6,7])|([7][^9])|([9][1,8,9]))[\\d]{8}$"),
    PHONE_REGEX_MOBILE_AREA("^[0][0-9]{2,3}-[0-9]{5,10}$"),
    PHONE_REGEX_MOBILE_NOT_AREA("^[1-9]{1}[0-9]{5,8}$");

    private String regexVal;

    public String getRegexVal() {
        return regexVal;
    }


    PhoneRegex(String regexVal){
        this.regexVal = regexVal;
    }



    public static boolean validation(Integer types,String telephone){
        //1.校验types传值的类型
        boolean vailTypes = PhoneTypes.validation(types);
        if(!vailTypes){
            return false;
        }
        switch (types){
            //校验手机号码
            case 1:
                return vailPhone(telephone,PhoneRegex.PHONE_REGEX_TELEPHONE.getRegexVal());
            //校验固定号码
            case 2:
                if(telephone.length()>9){
                    return vailPhone(telephone,PhoneRegex.PHONE_REGEX_MOBILE_AREA.getRegexVal());
                }else{
                    return vailPhone(telephone,PhoneRegex.PHONE_REGEX_MOBILE_NOT_AREA.getRegexVal());
                }
            case 3:
                //手机号
                boolean mobileFlag = vailPhone(telephone,PhoneRegex.PHONE_REGEX_TELEPHONE.getRegexVal());
                //带区号的固定电话
                boolean mobileAreaFlag = vailPhone(telephone,PhoneRegex.PHONE_REGEX_MOBILE_AREA.getRegexVal());
                //不带区号的固定电话
                boolean mobileNotAreaFlag = vailPhone(telephone,PhoneRegex.PHONE_REGEX_MOBILE_NOT_AREA.getRegexVal());
                if(!mobileFlag&&!mobileAreaFlag&&!mobileNotAreaFlag){
                    return false;
                }
                return true;
            default:
                return false;
        }
    }


    private static boolean vailPhone(String phone,String regexVal){
        return phone.matches(regexVal);
    }



}
