package com.smproject.enums.checks;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author hu.liang
 * @Date 2019/6/27 0027
 */
@Target({ElementType.ANNOTATION_TYPE,ElementType.FIELD,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = VailTelephoneValidator.class)
public @interface VailTelephone {

    /**
     * 是否必填
     * @return
     */
    boolean require() default false;
    /**
     * 1.手机号 2.固定电话 3.不限
     */
    int telephoneType() default  1;
    /**
     * 默认的返回
     * @return
     */
    String message() default "号码输入有误!";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};



}
