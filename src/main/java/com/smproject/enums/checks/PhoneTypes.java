package com.smproject.enums.checks;

import java.util.Objects;

/**
 * @author hu.liang
 * @Date 2019/6/29 0029
 */
public enum PhoneTypes {

    /**
     * 1.只校验手机号  2.校验固定号码 3.所有
     */
    PHONE_TYPES_1(1,"手机号码"),
    PHONE_TYPES_2(2,"固定号码"),
    PHONE_TYPES_3(3,"所有");

    private Integer val;
    private String message;

    public Integer getVal() {
        return val;
    }

    public String getMessage() {
        return message;
    }

    PhoneTypes(Integer val, String message){
        this.val = val;
        this.message = message;
    }

    /**
     * 校验的方法
     * @param val
     * @return
     */
    public static boolean validation(Integer val){
        for(PhoneTypes types : PhoneTypes.values()){
            if(Objects.equals(types.getVal(),val)){
                return true;
            }
        }
        return false;
    }





}
