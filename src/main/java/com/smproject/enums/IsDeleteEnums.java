package com.smproject.enums;

/**
 * @author hu.liang
 * @Date 2019/6/27 0027
 */
public enum IsDeleteEnums {
    /**
     * 0.正常 1.删除
     */
    IS_DELETE_0(0),
    IS_DELETE_1(1);

    private Integer val;

    public Integer getVal() {
        return val;
    }

    IsDeleteEnums(Integer val){
        this.val = val;
    }

}
