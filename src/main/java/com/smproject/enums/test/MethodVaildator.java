package com.smproject.enums.test;

import com.smproject.exception.ModelVailException;

import javax.validation.Configuration;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

/**
 * @author hu.liang
 * @Date 2019/7/1 0001
 */
public class MethodVaildator implements ConstraintValidator<MethodVail,Object>{

    private String[] methods;

    private String message;


    @Override
    public void initialize(MethodVail methodVail) {
        this.methods = methodVail.methods();
        this.message =  methodVail.message();
    }

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        try {

        }catch (Exception e){
            throw new ModelVailException("");
        }
        return false;
    }
}
