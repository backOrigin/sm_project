package com.smproject.enums.test;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author hu.liang
 * @Date 2019/7/1 0001
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MethodVaildator.class )
public @interface MethodVail {

    String[] methods() ;

    String message();

    Class<? extends Payload>[] payload() default {};
}
