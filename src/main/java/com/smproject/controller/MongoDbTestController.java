package com.smproject.controller;

import cn.hutool.system.UserInfo;
import com.smproject.config.mongodb.entity.User;
import com.smproject.utils.MongoUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Meta;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author hu.liang
 * @Date 2019/7/4 0004
 */
@RestController
@RequestMapping(value = "/test")
public class MongoDbTestController {

    @Autowired
    private MongoUtils mongoUtils;

    @RequestMapping(value = "/saveMongo",method = RequestMethod.GET)
    public Map<String,Object> saveMongo(){
        //返回
        Map<String,Object> result = new HashMap<String,Object>();
        //请求参数
        Map<String,Object> param = new HashMap<>();
        param.put("name","jack");
        param.put("pageNo",1);
        param.put("pageSize",10);
        Map<String,Object> dataResult = this.mongoUtils.findPageList(param,User.class);
        result.put("status",true);
        result.put("data", dataResult.get("pageList"));
        result.put("dataTotal",dataResult.get("pageTotal"));
        return result;
    }

}
