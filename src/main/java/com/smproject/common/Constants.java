package com.smproject.common;

/**
 * 常量
 * @author hu.liang
 * @Date 2019/1/25 0025
 */
public class Constants {
    /**
     * 单位
     */
    public static Integer CONFIG_TYPE_11 = 11;
    /**
     * 个数
     */
    public static Integer CONFIG_TYPE_26 = 26;
    /**
     * 面积
     */
    public static Integer CONFIG_TYPE_21 = 21;
    /**
     * 米数
     */
    public static Integer CONFIG_TYPE_30 = 30;

}
