package com.smproject.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

/**
 * 返回类
 * @author hu.liang
 * @Date 2019/1/7 0007
 */
@Data
@AllArgsConstructor
public class ResultEntity<T> {

    /**
     * 返回类
     */
    private T result;
    /**
     * 状态
     */
    private boolean status;
    /**
     * 描述
     */
    private String msg;
    /**
     * 总数
     */
    private Long totalRecord;


    public ResultEntity(){}


    public ResultEntity(Boolean status, T result){
        this.status = status;
        this.result = result;
    }

}
