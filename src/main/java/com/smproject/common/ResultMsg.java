package com.smproject.common;

/**
 * 接口请求的返回
 * @author hu.liang
 * @Date 2019/1/10 0010
 */

public enum ResultMsg {
    /**
     * 该接口调用成功
     */
    MSG_OK_200("该接口调用成功！"),
    /**
     * 接口调用失败
     */
    MSG_BAD_REQUEST_400("接口调用失败！"),
    /**
     * 本次接口调用，验证授权信息失败，请重新登录获取授权信息！
     */
    MSG_UNAUTHORIZED_401("本次接口调用，验证授权信息失败，您尚未登录或无权限访问此功能！"),
    /**
     * 本次接口调用，验证授权信息失败，请重新登录获取授权信息！
     */
    MSG_UNAUTHORIZED_402("本次接口调用，access_token已过期，请刷新！"),
    /**
     * 接口服务出现问题，请联系系统管理员！
     */
    MSG_INTERNAL_SERVER_ERROR_500("接口服务出现问题，请联系系统管理员！");

    private String info;

    private ResultMsg(String info){
        this.info=info;
    }

    @Override
    public String toString(){return this.info;}
}
