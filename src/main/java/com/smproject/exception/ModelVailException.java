package com.smproject.exception;

import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author hu.liang
 * @Date 2019/1/11 0011
 */
public class ModelVailException extends RuntimeException{

    /**
     * 实体校验异常
     * @param msg
     */
    public ModelVailException(String msg){
        super(msg);
    }
}
