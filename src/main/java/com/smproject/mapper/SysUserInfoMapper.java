package com.smproject.mapper;


import com.smproject.entity.SysUserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author hu.liang
 * @Date 2019/3/19 0019
 */
@Mapper
public interface SysUserInfoMapper {

    /**
     * 查询用户列表
     * @param name
     * @return
     */
    List<SysUserInfo> findUserList(String name);

    /**
     * 根据id查询数据的详情
     * @param userId
     * @return
     */
    SysUserInfo findOne(Integer userId);

    /**
     * 根据登录名查询数据的详情
     * @param userName
     * @return
     */
    SysUserInfo findByUserName(String userName);

    SysUserInfo findByLoginName(String loginName);
}
