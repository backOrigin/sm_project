package com.smproject.config;

import com.smproject.SmProjectApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;

/**
 * 配置类
 * @author hu.liang
 * @Date 2018/10/13 0013
 */
@Configuration
public class SpringApplicationBuilderConfig extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(SmProjectApplication.class);
    }


}
