package com.smproject.config.mongodb.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author hu.liang
 * @Date 2019/7/4 0004
 */
@Document
@Data
public class User {
    @Id
    private Integer id;
    private String name;
    private String sex;
}
