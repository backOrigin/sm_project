package com.smproject.config.mongodb;

import com.mongodb.MongoClientOptions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * mongodb配置
 * @author hu.liang
 * @Date 2019/4/2 0002
 */
@Slf4j
@Configuration
public class MongoDbSettings {

    MongoDbSettings(){
        log.info("--------配置mongoDB成功!-------------");
    }

    @Bean
    public MongoClientOptions mongoOptions() {
        return MongoClientOptions
                .builder()
                .maxConnectionIdleTime(60000)
                .build();
    }

}
