package com.smproject.config.rabbitmq.receiver;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 处理消息
 * @author hu.liang
 * @Date 2019/7/5 0005
 */
@Slf4j
@Component
public class Customer {

    @RabbitListener(queues = {"test-queue","test-customer-queue"}, containerFactory = "rabbitListenerContainerFactory")
    public void handleStudentMessage(String message) throws Exception {
        // 处理消息
        log.info("测试处理信息成功,接收到的:"+message);
    }

}
