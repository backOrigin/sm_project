package com.smproject.config.rabbitmq.send;

/**
 * 发送消息的枚举
 * @author hu.liang
 * @Date 2019/7/5 0005
 */
public enum RabbitMqEnums {
    /**
     * 发送消息者
     */
    TEST_ENUMS("test");

    private String val;

    public String getVal() {
        return val;
    }

    RabbitMqEnums(String val){
        this.val = val;
    }

}
