package com.smproject.config.rabbitmq;

import org.springframework.amqp.core.DirectExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 交换机的配置
 * @author hu.liang
 * @Date 2019/3/28 0028
 */
@Configuration
public class ExchangeConfig {


    @Bean
    public DirectExchange directExchange(){
        return new DirectExchange(RabbitMqConfig.SM_PROJECT_EXCHANGE,true,false);
    }

}
