package com.smproject.config.rabbitmq;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;

/**
 * 回调的配置
 * @author hu.liang
 * @Date 2019/3/28 0028
 */
@Slf4j
public class MsgSendConfirmCallBack implements RabbitTemplate.ConfirmCallback {
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        log.info("MsgSendConfirmCallBack, 回调id:" + correlationData);
        if (ack) {
           log.info("消息消费成功");
        } else {
            log.info("消息消费失败:" + cause+"\n重新发送");
        }
    }
}
