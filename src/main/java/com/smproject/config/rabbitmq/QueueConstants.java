package com.smproject.config.rabbitmq;

/**
 * @author hu.liang
 * @Date 2019/5/20 0020
 */
public class QueueConstants {
    /**
     * 测试队列
     */
    public static final String TEST_KEY = "queue_test";
    public static final String TEST_CUSTOMER_KEY = "queue_test_costomer";
}
