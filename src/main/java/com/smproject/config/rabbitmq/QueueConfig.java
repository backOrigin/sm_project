package com.smproject.config.rabbitmq;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 队列的配置
 * @author hu.liang
 * @Date 2019/3/28 0028
 */
@Configuration
public class QueueConfig {

    @Autowired
    private ExchangeConfig exchangeConfig;

    @Bean
    public Queue testQueue() {
        return new Queue("test-queue",true,false,false);
    }

    @Bean
    public Binding bindingTest(){
        return BindingBuilder.bind(testQueue()).to(exchangeConfig.directExchange()).with(QueueConstants.TEST_KEY);
    }

    @Bean
    public Queue testCustomer(){
        return new Queue("test-customer-queue",true,false, false);
    }

    @Bean
    public Binding bindingTestCustomer(){
        return BindingBuilder.bind(testCustomer()).to(exchangeConfig.directExchange()).with(QueueConstants.TEST_CUSTOMER_KEY);
    }

}
