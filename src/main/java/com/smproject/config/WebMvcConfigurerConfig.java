package com.smproject.config;

import com.smproject.config.interceptor.RedisSessionInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.velocity.VelocityLayoutView;
import org.springframework.web.servlet.view.velocity.VelocityLayoutViewResolver;
import org.springframework.web.servlet.view.velocity.VelocityViewResolver;

/**
 * 静态资源访问
 * @author hu.liang
 * @Date 2018/10/13 0013
 */
@Configuration
public class WebMvcConfigurerConfig extends WebMvcConfigurerAdapter {


    @Bean
    public RedisSessionInterceptor getSessionInterceptor(){
        return new RedisSessionInterceptor();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }


    @Override
    public void addViewControllers(ViewControllerRegistry registry){
        registry.addViewController("/").setViewName("forward:/api/user/findLoginPage");
        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
        super.addViewControllers(registry);
    }


    @Bean
    public VelocityLayoutViewResolver viewResolver(){
        VelocityLayoutViewResolver viewResolver= new VelocityLayoutViewResolver();
        //配置的是项目编译之后,视图所在的路径
        viewResolver.setPrefix("/templates");
        viewResolver.setSuffix(".vm");
        viewResolver.setViewClass(VelocityLayoutView.class);
        //viewResolver.setViewClass(VelocityLayoutView.class);
        return viewResolver;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(getSessionInterceptor()).addPathPatterns("/api/**")
            .excludePathPatterns("/api/user/login");
        super.addInterceptors(registry);
    }

}
