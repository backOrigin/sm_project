package com.smproject.config;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;

import java.util.Date;
import java.util.Map;

/**
 * @author hu.liang
 * @Date 2018/10/13 0013
 */
public class CommonLog {

    private Date callTime;
    private String url;
    private String httpMethod;
    private String classMethod;
    private String ip;
    private String args;
    private String result;
    private Long time;
    private String exception;
    private Map<String, String> options;
    private String desc;






    @Override
    public String toString() {
        StringBuffer optionStr = new StringBuffer();
        if (ObjectUtil.isNotNull(this.options)) {
            optionStr.append(JSONUtil.toJsonStr(optionStr));
        }

        String info = "\ncallTime:{}\nurl:{}\nhttpMethod:{}\nclassMethod:{}\nip:{}\nargs:{}\nresult:{}\ntime:{}\nexception:{}\ndesc:{}\noption:{}\n";
        return StrUtil.format(info, new Object[]{DateUtil.format(this.callTime, "yyyy-MM-dd HH:mm:ss"), this.url, this.httpMethod, this.classMethod, this.ip, this.args, this.result, this.time, this.exception, this.desc, optionStr.toString()});
    }

    public String toHtmString() {
        StringBuffer optionStr = new StringBuffer();
        if (ObjectUtil.isNotNull(this.options)) {
            optionStr.append(JSONUtil.toJsonStr(optionStr));
        }

        String info = "<br/>callTime:{}<br/>url:{}<br/>httpMethod:{}<br/>classMethod:{}<br/>ip:{}<br/>args:{}<br/>result:{}<br/>time:{}<br/>exception:{}<br/>desc:{}<br/>option:{}<br/>";
        return StrUtil.format(info, new Object[]{DateUtil.format(this.callTime, "yyyy-MM-dd HH:mm:ss"), this.url, this.httpMethod, this.classMethod, this.ip, this.args, this.result, this.time, this.exception, this.desc, optionStr.toString()});
    }

    public CommonLog() {
    }

    public Date getCallTime() {
        return this.callTime;
    }

    public String getUrl() {
        return this.url;
    }

    public String getHttpMethod() {
        return this.httpMethod;
    }

    public String getClassMethod() {
        return this.classMethod;
    }

    public String getIp() {
        return this.ip;
    }

    public String getArgs() {
        return this.args;
    }

    public String getResult() {
        return this.result;
    }

    public Long getTime() {
        return this.time;
    }

    public String getException() {
        return this.exception;
    }

    public Map<String, String> getOptions() {
        return this.options;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setCallTime(Date callTime) {
        this.callTime = callTime;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public void setClassMethod(String classMethod) {
        this.classMethod = classMethod;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setArgs(String args) {
        this.args = args;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public void setOptions(Map<String, String> options) {
        this.options = options;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
