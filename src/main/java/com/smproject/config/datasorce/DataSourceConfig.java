package com.smproject.config.datasorce;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * 配置数据源
 * @author hu.liang
 * @Date 2019/6/27 0027
 */
@Configuration
public class DataSourceConfig {


    @Bean
    @ConfigurationProperties("mysql.masterdataSource")
    @Primary
    public DataSource masterDataSource(){
        return DataSourceBuilder.create().build();
    }



    @Bean(name = "myRountingDataSource")
    public DataSource myRountingDataSource(@Qualifier("masterDataSource") DataSource masterDataSource){
        Map<Object,Object> targetDataSources = new HashMap<>();
        targetDataSources.put(DataSourceType.MASTER,masterDataSource);

        MyRountingDataSource myRountingDataSource = new MyRountingDataSource();
        myRountingDataSource.setDefaultTargetDataSource(masterDataSource);
        myRountingDataSource.setTargetDataSources(targetDataSources);
        return myRountingDataSource;
    }



}
