package com.smproject.config.datasorce;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author hu.liang
 * @Date 2019/6/27 0027
 */
@Slf4j
public class DataSourceContextHolder {

    private static final ThreadLocal<DataSourceType> contextHolder = new ThreadLocal<>();

    public static final AtomicInteger counter = new AtomicInteger(-1);

    public static void set(DataSourceType dbType){
        contextHolder.set(dbType);
    }


    public static DataSourceType get(){
        return contextHolder.get();
    }

    public static void remove(){ contextHolder.remove();}


    public static void master(){
        set(DataSourceType.MASTER);
    }

    public static void slave(){
        set(DataSourceType.SLAVE);
    }

    public static void clearDataSource() {
        remove();
    }
}
