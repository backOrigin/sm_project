package com.smproject.config.datasorce;

/**
 * @author hu.liang
 * @Date 2019/6/27 0027
 */
public enum DataSourceType {
    /**
     * 主从数据库
     */
    MASTER,
    SLAVE,
}
