package com.smproject;

import com.smproject.config.datasorce.DataSourceContextHolder;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Import;

import javax.swing.*;

/**
 * 启动类
 * @author hu.liang
 * @Date 2019/3/19 0019
 */
@EnableAutoConfiguration
@MapperScan(value = "com.smproject.mapper")
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class SmProjectApplication {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(SmProjectApplication.class);
        app.setWebEnvironment(true);
        app.run(args);
    }

}
