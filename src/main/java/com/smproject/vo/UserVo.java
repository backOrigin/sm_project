package com.smproject.vo;

import com.smproject.enums.checks.VailTelephone;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author hu.liang
 * @Date 2019/6/27 0027
 */
@Data
public class UserVo implements Serializable {
    private static final long serialVersionUID = -391674038240071192L;

    @NotBlank(message = "不能为空!")
    @VailTelephone(telephoneType = 3)
    private String phone;

}
