package com.smproject.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * 用户表
 * @author hu.liang
 * @Date 2019/3/19 0019
 */
public class SysUserInfo implements Serializable{
    private static final long serialVersionUID = -6234913808202425343L;
    /**
     * 用户id
     */
    private Integer userId;
    /**
     * 登录名
     */
    private String loginName;
    /**
     * 密码
     */
    private String password;
    /**
     * 用户真实姓名
     */
    private String userName;
    /**
     * 性别
     */
    private String sex;
    /**
     * 用户类型
     */
    private Integer type;
    /**
     * 出生年月日
     */
    private Date birthday;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 证件号
     */
    private String idCard;
    /**
     * 删除标记
     */
    private String isDelete;
    /**
     * 商家id
     */
    private String shopId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }
}
