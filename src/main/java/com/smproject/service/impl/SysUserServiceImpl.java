package com.smproject.service.impl;

import com.smproject.entity.SysUserInfo;
import com.smproject.mapper.SysUserInfoMapper;
import com.smproject.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hu.liang
 * @Date 2019/3/19 0019
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    private SysUserInfoMapper userInfoMapper;

    @Override
    public List<SysUserInfo> queryUserList() {
        return this.userInfoMapper.findUserList("三");
    }

    @Override
    public SysUserInfo findOne(Integer userId) {
        return this.userInfoMapper.findOne(userId);
    }

    @Override
    public SysUserInfo findByUserName(String userName) {
        return this.userInfoMapper.findByUserName(userName);
    }

    @Override
    public SysUserInfo findByLoginName(String loginName) {
        return this.userInfoMapper.findByLoginName(loginName);
    }

}
