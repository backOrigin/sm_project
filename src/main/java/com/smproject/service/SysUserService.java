package com.smproject.service;

import com.smproject.entity.SysUserInfo;

import java.util.List;

/**
 * 用户service
 * @author hu.liang
 * @Date 2019/3/19 0019
 */
public interface SysUserService {

    List<SysUserInfo> queryUserList();

    SysUserInfo findOne(Integer userId);

    SysUserInfo findByUserName(String userName);

    SysUserInfo findByLoginName(String loginName);
}
