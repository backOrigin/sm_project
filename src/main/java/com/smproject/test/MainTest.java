package com.smproject.test;

import com.smproject.enums.test.UseCase;
import com.smproject.utils.PasswordUtils;

import java.lang.reflect.Method;
import java.util.*;

/**
 * @author hu.liang
 * @Date 2019/7/1 0001
 */
public class MainTest {


    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        Collections.addAll(list,"47","48","41","a");
        vailNums(list,PasswordUtils.class);
    }

    public static void vailNums(List<String> list,Class<?> c1){
        for(Method method : c1.getDeclaredMethods()){
            UseCase useCase = method.getAnnotation(UseCase.class);
            if(Objects.nonNull(useCase)){
                String[] ids = useCase.id();
                if(Objects.nonNull(ids)&&ids.length>0){
                    for(String id : ids){
                        System.out.println("找到可以查询到的:"+id+"--信息："+useCase.description());
                        list.remove(id);
                    }
                }
            }
        }
        for(String i : list){
            System.out.println("case-"+i);
        }
    }


}
