package com.smproject.utils;

import com.smproject.config.rabbitmq.QueueConstants;
import com.smproject.config.rabbitmq.RabbitMqConfig;
import com.smproject.config.rabbitmq.send.RabbitMqEnums;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * 发送消息的工具类
 * @author hu.liang
 * @Date 2019/7/5 0005
 */
@Component
public class RabbitMqUtils {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 判断用哪个队列发送消息
     * @param mqName
     * @param objs
     * @return
     */
    public Map<String,Object> sender(String mqName,List<Object> objs){
        Map<String,Object> resultMap = new HashMap<>();
        //测试的队列
        if(Objects.equals(mqName, RabbitMqEnums.TEST_ENUMS.getVal())){
            for(Object obj : objs ){
                CorrelationData correlationId = new CorrelationData(UUID.randomUUID().toString());
                rabbitTemplate.convertAndSend(RabbitMqConfig.SM_PROJECT_EXCHANGE, QueueConstants.TEST_KEY,
                        obj, correlationId);
                //设置休眠时间
                sleeepThrea();
            }
            resultMap.put("status",true);
            resultMap.put("msg","调用成功");
            return resultMap;
        }
        return null;
    }

    /**
     * 休眠时间
     */
    private void sleeepThrea(){
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
