package com.smproject.utils;

import com.smproject.enums.test.UseCase;

/**
 * @author hu.liang
 * @Date 2019/7/1 0001
 */
public class PasswordUtils {

    @UseCase(id = {"44","46","a"},description = "密码必须小写")
    public boolean validatePassword(String password){
        return password.matches("\\w*\\d\\w*");
    }

    @UseCase(id = { "48", "49", "50"})
    public String encyptPassword(String password){
        return new StringBuffer(password).reverse().toString();
    }

}
